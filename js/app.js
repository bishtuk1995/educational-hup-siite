$(document).ready(function () {
    console.log('okkkkkkkk')

    $(".fa-bars").click(function () {
        $(this).toggleClass("fa-times");
        $(".navbar").toggleClass("navbar__toggle");
    })

    $(window).on("load scroll", function () {
        $(".fa-bars").removeClass("fa-times");
        $(".navbar").removeClass("navbar__toggle");



        if ($(window).scrollTop() > 40) {
            $('.header').addClass("header__shadow");
        } else {
            $('.header').removeClass("header__shadow");
        }

    });


 

    $(window).on('load scroll', function () {
        $('section').each(function (index, element) {
             let top=  $(window).scrollTop() ;
             let height= $(this).height();
             let offset= $(this).offset().top - 200;
             let id= $(this).attr('id');

             if (top>= offset && top < (height+offset)) {
                 $(".navbar ul li a").removeClass("active");
                 $('.navbar').find(`[href="#${id}"]`).addClass("active");
             }
        });
    });
  

    //animation using anime.js
    // Wrap every letter in a span
    var textWrapper = document.querySelector('.ml10 .letters');
    textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");

    anime.timeline({ loop: true })
        .add({
            targets: '.ml10 .letter',
            rotateY: [-90, 0],
            duration: 1300,
            delay: (el, i) => 45 * i
        }).add({
            targets: '.ml10',
            opacity: 0,
            duration: 1000,
            easing: "easeOutExpo",
            delay: 1000
        });



    /*   header  heading Animation */
    // Wrap every letter in a span
    var textWrapper = document.querySelector('.ml11 .letters');
    textWrapper.innerHTML = textWrapper.textContent.replace(/([^\x00-\x80]|\w)/g, "<span class='letter'>$&</span>");

    anime.timeline({ loop: true })
        .add({
            targets: '.ml11 .line',
            scaleY: [0, 1],
            opacity: [0.5, 1],
            easing: "easeOutExpo",
            duration: 700
        })
        .add({
            targets: '.ml11 .line',
            translateX: [0, document.querySelector('.ml11 .letters').getBoundingClientRect().width + 10],
            easing: "easeOutExpo",
            duration: 700,
            delay: 100
        }).add({
            targets: '.ml11 .letter',
            opacity: [0, 1],
            easing: "easeOutExpo",
            duration: 600,
            offset: '-=775',
            delay: (el, i) => 34 * (i + 1)
        }).add({
            targets: '.ml11',
            opacity: 0,
            duration: 1000,
            easing: "easeOutExpo",
            delay: 1000
        });
    /*   header  heading Animation */



    // <!-- Initialize Swiper -->
      var swiper = new Swiper(".mySwiper", {
        slidesPerView: 3,
        spaceBetween: 30,
        pagination: {
          el: ".swiper-pagination",
          dynamicBullets: true,
        },
        autoplay: {
            delay: 5000,
          },

        breakpoints: {
            // when window width is >= 320px
            320: {
              slidesPerView: 1,
              spaceBetween: 20
            },
            // when window width is >= 480px
            480: {
              slidesPerView: 2,
              spaceBetween: 30
            },
            // when window width is >= 640px
            640: {
              slidesPerView: 3,
              spaceBetween: 40
            }
          }


      });



});






